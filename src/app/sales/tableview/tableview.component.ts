import { Component, OnInit } from '@angular/core';
import { LocalDateService } from 'src/app/core/local-date.service';

@Component({
  selector: 'app-tableview',
  templateUrl: './tableview.component.html',
  styleUrls: ['./tableview.component.scss']
})
export class TableviewComponent implements OnInit {
  months: any;
  years: any;
  month: number;
  year: number;
  ready = false;
  constructor(private localDates: LocalDateService) { }
  applyFilter() {
    this.ready = false;
    setTimeout(() => {
      this.ready = true;
    }, 1000);
  }
  clear() {
    this.ready = false;
    this.month = 0;
    this.year = 0;
    this.applyFilter();
  }
  ngOnInit(): void {
    this.months = this.localDates.getMonths();
    this.years = this.localDates.getYears();
    this.ready = true;
  }

}
