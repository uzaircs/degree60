import { Injectable, Inject, InjectionToken } from '@angular/core';

import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService, StorageTranscoder } from 'ngx-webstorage-service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Sale, SaleItem } from './sale';
import { environment } from 'src/environments/environment';
const STORAGE_KEY = 'db-Sale';
export const SALE_STORAGE_SERVICE =
  new InjectionToken<StorageService>(STORAGE_KEY);
@Injectable({
  providedIn: 'root'
})
class SaleTranscoder implements StorageTranscoder<Sale[]> {
  encode(value: Sale[]): string {
    return JSON.stringify(value);
  }
  decode(value: string): Sale[] {

    return Object.assign(new Array<Sale>(), JSON.parse(value));

  }
}

@Injectable()
export class SalesStoreService {
  baseurl = environment.api;
  sales: Sale[] = [];
  private SaleStorage: StorageService<Sale[]>;
  constructor(@Inject(SALE_STORAGE_SERVICE) private storage: StorageService, private http: HttpClient) {
    this.SaleStorage = storage.withDefaultTranscoder(new SaleTranscoder());
  }
  getSales(month = 0, year = 0): Observable<Sale[]> {

    if (this.SaleStorage.get(STORAGE_KEY)) {
      const sales = new BehaviorSubject<Sale[]>([]);
      if (!month && !year) {
        sales.next(this.SaleStorage.get(STORAGE_KEY));
      }
      let url = this.baseurl + `/api/Payments?month=${month}&year=${year}`;
      this.http.get<Sale[]>(url, { withCredentials: true }).subscribe(s => {
        sales.next(s);
        this.SaleStorage.clear();
        this.SaleStorage.set(STORAGE_KEY, s);
      });
      return sales.asObservable();
    }
    this.sync();
    return this.http.get<Sale[]>(this.baseurl + '/api/Payments', { withCredentials: true });
  }
  getSalesTable(month = 0, year = 0): Observable<SaleItem[]> {
    const sales = new BehaviorSubject<SaleItem[]>(null);
    this.http.get<SaleItem[]>(this.baseurl + '/api/salestable?month=' + month + '&year=' + year, { withCredentials: true }).toPromise().then((response) => {
      if (response && Array.isArray(response)) {
        sales.next(response);
      }
    })
    return sales;
  }
  getSalesTableByUser(id: number): Observable<SaleItem[]> {
    const sales = new BehaviorSubject<SaleItem[]>(null);
    this.http.get<SaleItem[]>(this.baseurl + '/api/salestable/user/' + id, { withCredentials: true }).toPromise().then((response) => {
      if (response && Array.isArray(response)) {
        sales.next(response);
      }
    })
    return sales;
  }
  getSalesTableProject(id: number): Observable<SaleItem> {
    const sales = new BehaviorSubject<SaleItem>(null);
    this.http.get<SaleItem>(this.baseurl + '/api/salestable/' + id, { withCredentials: true }).toPromise().then((response) => {
      if (response.Id) {
        sales.next(response);
      }
    })
    return sales;
  }
  deleteSale(id: number) {
    this.http.delete(this.baseurl + '/api/Payments/' + id, { withCredentials: true });
    this.sync();
  }
  getSalebyAccount(id: number) {
    return this.http.get<Sale[]>(this.baseurl + '/api/Payments/' + id, { withCredentials: true });
  }
  updateSale(sale: Sale): Observable<Sale> {
    return this.http.put<Sale>(this.baseurl + '/api/Payments' + sale.target_id, sale, { withCredentials: true });
  }
  addSale(sale: Sale): Observable<Sale> {

    return this.http.post<Sale>(this.baseurl + '/api/Payments', sale, { withCredentials: true });
  }
  sync() {
    this.http.get<Sale[]>(this.baseurl + '/api/Payments',
      { withCredentials: true }).subscribe(sales => this.SaleStorage.set(STORAGE_KEY, sales));
  }
}
