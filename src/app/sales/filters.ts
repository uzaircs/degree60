export class Filters {
    month: number;
    startDate: Date;
    endDate: Date;
}
