import { Component, Input, OnInit } from '@angular/core';
import { Project } from 'src/app/shared/project.model';
import { SaleItem } from '../sale';
import { SalesStoreService } from '../sales-store.service';
export interface UserSales {
  user: string;
  user_id: number;
  projects?: SaleItem[];
}
@Component({
  selector: 'app-sales-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  sales: SaleItem[] = [];
  ready = false;
  user_sales: UserSales[] = [];
  @Input() month;
  @Input() year;
  constructor(
    private saleStore: SalesStoreService
  ) { }

  ngOnInit(): void {
    this.saleStore.getSalesTable(this.month, this.year).subscribe((sales) => {
      if (sales && Array.isArray(sales)) {
        this.sales = sales;
        this.user_sales.push({
          user: 'All Projects',
          user_id: 0,
          projects: sales
        })
        var owners: UserSales[] = sales
          .map(project => { return { owner: project.sales_user_id, name: project.salesPerson } })
          .filter((owner, index, arr) =>
            arr.
              findIndex(o => o.owner === owner.owner) === index).map(userProject => {
                let userProjects = sales.filter(project => project.sales_user_id === userProject.owner);
                return {
                  user_id: userProject.owner,
                  user: userProject.name,
                  projects: userProjects
                }
              });

        this.user_sales = [...this.user_sales, ...owners];

        this.ready = true;
      }
    })
  }

}
