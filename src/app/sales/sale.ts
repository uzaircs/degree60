import { Account } from '../accounts/account';
import { Project } from '../shared/project.model';

export class Sale {
  account: Account;
  account_id: number;
  TargetMonth: number;
  TargetYear: number;
  target_id: number;
  amount_completed: number;
  amount: number;
  created_at: Date;

  user: number;
  projects: Project[];
}
export class SaleItem {
  title: string;
  Id: number;
  salesPerson: string;
  total: number;
  received: number;
  remaining: number;
  sales_user_id: number
}
