import { TestBed } from '@angular/core/testing';

import { SalesStoreService } from './sales-store.service';

describe('SalesStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SalesStoreService = TestBed.get(SalesStoreService);
    expect(service).toBeTruthy();
  });
});
