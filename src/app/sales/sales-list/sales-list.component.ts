import { Component, OnInit, Input } from '@angular/core';
import { Filters } from '../filters';
import { SelectItem } from 'src/app/shared/SelectItem';
import { AccountStoreService } from 'src/app/accounts/account-store.service';
import { Account } from 'src/app/accounts/account';
import { SalesStoreService } from '../sales-store.service';
import { Sale } from '../sale';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { LocalDateService } from 'src/app/core/local-date.service';

@Component({
  selector: 'app-sales-list',
  templateUrl: './sales-list.component.html',
  styleUrls: ['./sales-list.component.scss']
})

export class SalesListComponent implements OnInit {
  dates: Array<SelectItem>;
  @Input() SummaryView: string;
  sales: Sale[];
  filter: Filters;
  summary: Sale;
  month: number = new Date().getMonth() + 1;
  year: number = new Date().getFullYear();
  months: SelectItem[];
  years: SelectItem[];
  sortOptions: SelectItem[];
  busy = true;
  sort: number;
  status: number;
  constructor(private saleStore: SalesStoreService, private toastr: ToastrService, localDates: LocalDateService) {
    this.filter = new Filters();
    this.dates = new Array<SelectItem>();
    this.months = localDates.getMonths();
    this.years = localDates.getYears();
  }
  applyFilter() {
    this.busy = true;
    this.saleStore.getSales(this.month, this.year).subscribe(sales => {
      this.sales = sales;

      if (this.sales.length) {
        this.sales = this.sales.filter(sale => {
          return sale.TargetMonth === this.month && sale.TargetYear === this.year;
        });
        if (this.sort > -1) {
          this.sales.sort((a, b) => {
            if (this.sort === 0) {
              return a.amount - b.amount;
            }
            if (this.sort === 1) {
              if (a.amount_completed && b.amount_completed) {
                return a.amount_completed - b.amount_completed;
              } else if (a.amount_completed) {
                return a.amount_completed - 0;
              } else if (b.amount_completed) {
                return 0 - b.amount_completed;
              } else {
                return 0;
              }
            }
            if (this.sort === 2) {
              if (a.amount_completed && b.amount_completed) {
                return (a.amount - a.amount_completed) - (b.amount - b.amount_completed);
              }
            }
          });
        }
        this.toastr.success('Filter Applied');
        this.getSummary();
      }

    });

  }
  getSummary() {
    this.summary = new Sale();
    this.summary.account = new Account();
    this.summary.account.name = moment(new Date(this.year, this.month - 1)).format('MMM YYYY');
    this.summary.projects = [].concat.apply([], this.sales.map(s => s.projects));
    if (this.sales.length) {
      this.summary.amount = this.sales.map(s => s.amount).reduce((a, b) => a + b);
      /*   this.summary.amount_completed = this.sales.map(s => s.amount_completed).reduce((a, b) => a + b); */
      this.summary.amount_completed = this.sales.filter(a => a.amount_completed > 0).map(a => a.amount_completed).reduce((a, b) => a + b);
    } else {
      this.summary.amount = 0;
      this.summary.amount_completed = 0;
      this.summary.amount_completed = 0;
    }
    this.summary.TargetMonth = this.month;
    this.summary.TargetYear = this.year;
    this.busy = false;
  }
  ngOnInit() {
    this.saleStore.getSales().subscribe(sales => {
      this.sales = sales;

      this.getSummary();
    });
    this.sortOptions = new Array<SelectItem>();
    ['Target', 'Target Completed', 'Remaining', 'Projects Completed'].map((s, i) => {
      const status = new SelectItem(i, s);
      this.sortOptions.push(status);
    });

  }

}
