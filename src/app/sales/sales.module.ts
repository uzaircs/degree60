import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesListComponent } from './sales-list/sales-list.component';
import { CoreModule } from '../core/core.module';
import { MomentModule } from 'ngx-moment';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SALE_STORAGE_SERVICE, SalesStoreService } from './sales-store.service';
import { SESSION_STORAGE } from 'ngx-webstorage-service';
import { PaymentModule } from '../payment/payment.module';
import { PipesModule } from '../core/pipes/pipes.module';
import { TableModule } from 'primeng/table';
import { TableComponent } from './table/table.component';
import { TableviewComponent } from './tableview/tableview.component';
import { TabViewModule } from 'primeng/tabview';
@NgModule({
  declarations: [SalesListComponent, TableComponent, TableviewComponent],
  providers: [
    {
      provide: SALE_STORAGE_SERVICE,
      useExisting: SESSION_STORAGE
    },
    SalesStoreService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    CoreModule,
    MomentModule,
    NgxDropzoneModule,
    NgxDatatableModule,
    RouterModule,
    FormsModule,
    NgSelectModule,
    PaymentModule,
    PipesModule,
    TableModule,
    TabViewModule


  ],
})
export class SalesModule { }
