import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { fadeAnimation } from './animation';
import { UserStoreService } from './authorization/user-store.service';
import { User } from './authorization/user';
import { AuthService } from './authorization/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent {
  constructor(private primengConfig: PrimeNGConfig) { }
  ngOnInit() {
    this.primengConfig.ripple = true;

  }

}
