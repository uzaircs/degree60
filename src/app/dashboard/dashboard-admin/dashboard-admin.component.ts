import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/shared/project.model';
import { ProjectStoreService } from 'src/app/projects/project-store.service';
import { Sale } from 'src/app/sales/sale';
import { SalesStoreService } from 'src/app/sales/sales-store.service';
import { AuthService } from 'src/app/authorization/auth.service';
import { User } from 'src/app/authorization/user';
import { UserStoreService } from 'src/app/authorization/user-store.service';
import { KPI, V1Service } from 'src/app/kpi/v1.service';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.scss']
})
export class DashboardAdminComponent implements OnInit {
  sale: Sale;
  kpi: KPI;
  isBusy = false;
  projects: Project[];
  projectsDue: Project[];
  account: User;
  request = 0;
  constructor(
    private projectStorage: ProjectStoreService,
    private authStore: UserStoreService,
    private saleStore: SalesStoreService, private kpiV1: V1Service
  ) { }
  checkBusy() {
    if (this.request >= 2) {
      this.isBusy = false;
    } else {
      this.isBusy = true;
    }
  }
  ngOnInit() {
    this.account = this.authStore.getUser();
    /*  if (this.account.account.role == '2') {
    this.saleStore.getSalebyAccount(this.account.account.Id).subscribe(s => {


    });
  } */
    if (this.account.account.role == "1") {
      this.kpiV1.getKPI().then(kpi => {
        this.kpi = kpi;
        this.request++;
        this.checkBusy();

      })
    } else {
      this.request++; this.checkBusy();
    }



    this.projectStorage.getProjects().subscribe(p => {

      if (p) {
        this.request++;
        this.checkBusy();
        this.projects = p.filter(active => active.status === 0 || active.status === 1).sort((a, b) => {
          return +(new Date(b.deadline)) - (+new Date(a.deadline));
        }).sort((a, b) => {
          return +(new Date()) - (+new Date(a.deadline));
        }).slice(0, 5);
        this.projectsDue = p.filter(due => (+new Date(due.deadline) - +new Date()) < 0).slice(0, 5);
      }
    });
  }

}
