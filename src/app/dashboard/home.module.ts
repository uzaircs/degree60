import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { DashboardComponent } from './dashboard.component';
import { ProjectsModule } from '../projects/projects.module';
import { CustomersModule } from '../customers/customers.module';
import { RouterModule } from '@angular/router';
import { MomentModule } from 'ngx-moment';
import { DashboardAdminComponent } from './dashboard-admin/dashboard-admin.component';
import { DashboardTeamComponent } from './dashboard-team/dashboard-team.component';
import { UserStoreService, USER_STORAGE_SERVICE } from '../authorization/user-store.service';
import { SESSION_STORAGE } from 'ngx-webstorage-service';
import { KpiModule } from '../kpi/kpi.module';


@NgModule({
  declarations: [DashboardComponent, DashboardAdminComponent, DashboardTeamComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    CoreModule,
    ProjectsModule,
    CustomersModule,
    RouterModule,
    MomentModule,
    KpiModule
  ],
  entryComponents: [DashboardAdminComponent, DashboardTeamComponent],
  exports: [
    DashboardComponent
  ],
  providers: [

  ]

})
export class HomeModule { }
