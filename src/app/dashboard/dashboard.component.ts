import { Component, OnInit, ComponentFactoryResolver, ViewChild, ViewContainerRef } from '@angular/core';
import { Project } from '../shared/project.model';
import { ProjectStoreService } from '../projects/project-store.service';
import { isDate } from 'moment';
import { UserStoreService } from '../authorization/user-store.service';
import { DashboardAdminComponent } from './dashboard-admin/dashboard-admin.component';
import { DashboardTeamComponent } from './dashboard-team/dashboard-team.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor(private componentResolver: ComponentFactoryResolver, private userStorage: UserStoreService, private router: Router) { }
  @ViewChild('viewRef', { read: ViewContainerRef, static: true }) vcr: ViewContainerRef;
  ngOnInit() {
    var user = this.userStorage.getUser();
    if (user) {
      this.attachComponent(+user.account.role);
    } else {
      this.userStorage.RequestUser().toPromise().then(user => {
        if (user) {
          this.attachComponent(+user.role);
          this.userStorage.setUser({
            account: user,
            token: user.token
          });
        }
      });
    }
  }
  attachComponent(role: number) {
    if (+role === 1 || +role === 2) {
      this.vcr.createComponent(this.componentResolver.resolveComponentFactory(DashboardAdminComponent));
    } else if (role === 3) {
      this.vcr.createComponent(this.componentResolver.resolveComponentFactory(DashboardTeamComponent));
    } else {
      this.router.navigateByUrl('/login');
    }
  }
}
