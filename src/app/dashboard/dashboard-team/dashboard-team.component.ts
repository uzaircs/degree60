import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/shared/project.model';
import { ProjectStoreService } from 'src/app/projects/project-store.service';

@Component({
  selector: 'app-dashboard-team',
  templateUrl: './dashboard-team.component.html',
  styleUrls: ['./dashboard-team.component.scss']
})
export class DashboardTeamComponent implements OnInit {
  projects: Project[];
  projectsDue: Project[];
  constructor(private projectStorage: ProjectStoreService) { }

  ngOnInit() {
    this.projectStorage.getProjects().subscribe(p => {

      if (p) {
        this.projects = p.filter(active => active.status === 0 || active.status === 1).sort((a, b) => {
          return +(new Date(b.deadline)) - (+new Date(a.deadline));
        }).sort((a, b) => {
          return +(new Date()) - (+new Date(a.deadline));
        }).slice(0, 5);
        this.projectsDue = p.filter(due => (+new Date(due.deadline) - +new Date()) < 0).slice(0, 5);
      }
    });
  }

}
