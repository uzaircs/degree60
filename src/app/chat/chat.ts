export class Chat {

    Id: number;
    message: string;
    name: string;
    self: boolean;
    project: number;
    account: number;
    created_at: Date
}
