import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Chat } from './chat';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChatStoreService {
  baseurl = environment.api;
  constructor(private http: HttpClient) { }
  getMessages(project: number): Observable<Chat[]> {
    return this.http.get<Chat[]>(this.baseurl + '/api/Interaction/GetProject/' + project, { withCredentials: true });
  }
  AddMessage(message: Chat): Observable<Chat> {
    return this.http.post<Chat>(this.baseurl + '/api/Interaction/' + message.project, message, { withCredentials: true });
  }
  getAccountMessages(account: number): Observable<Chat[]> {

    return this.http.get<Chat[]>(this.baseurl + '/api/Interaction/GetAccount/' + account, { withCredentials: true });
  }
}
