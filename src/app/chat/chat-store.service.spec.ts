import { TestBed } from '@angular/core/testing';

import { ChatStoreService } from './chat-store.service';

describe('ChatStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChatStoreService = TestBed.get(ChatStoreService);
    expect(service).toBeTruthy();
  });
});
