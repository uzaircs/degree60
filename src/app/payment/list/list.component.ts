import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from 'primeng/api';
import { ApiService } from '../api.service';
import { Payment } from '../model';

@Component({
  selector: 'app-payment-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  payments: Payment[];
  ready = false;
  no_payments = false;
  context: Payment = null;
  @Input() month;
  @Input() year;
  display = false;
  constructor(
    private paymentsStorage: ApiService,
    private confirmationService: ConfirmationService,
    private toastr: ToastrService
  ) {

  }
  savePayment() {
    this.display = false;
    this.paymentsStorage.editPayment(this.context.Id, this.context).then(result => {
      let index = this.payments.findIndex(c => c.Id == this.context.Id);
      if (index > -1) {
        this.payments[index] = { ...this.context }
      }
    })
  }
  showDialog(payment_id) {
    let payment = this.payments.find(c => c.Id === payment_id);
    if (payment) {
      this.context = payment;
    }
    this.display = true;
  }
  deletePayment = (id) => {
    let index = this.payments.findIndex(p => p.Id === id);

    if (index > -1) {
      this.paymentsStorage.deletePayment(id).then(() => {
        this.payments.splice(index, 1);
        this.toastr.success('Payment deleted successfully');
      }).catch((error) => {
        this.toastr.error('There was an error while deleting the payment');
      })
    }
  }
  confirmDelete = (event: Event, payment_id) => {
    this.confirmationService.confirm({
      target: event.target,
      message: 'Are you sure you want to delete this payment?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {

        this.deletePayment(payment_id);
      },
      reject: () => {
        //reject action
      }
    });
  }
  ngOnInit() {
    this.paymentsStorage.getPayments(this.month, this.year).subscribe((payments) => {
      if (payments.length) {
        this.payments = payments;
        this.ready = true;
      } else {

      }

    }, () => {
      this.no_payments = true;
    })
  }

}
