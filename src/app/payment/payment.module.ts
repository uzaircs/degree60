import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { MomentModule } from 'ngx-moment';
import { CoreModule } from '../core/core.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ApiService, PAYMENT_STORAGE_SERVICE } from './api.service';
import { SESSION_STORAGE } from 'ngx-webstorage-service';
import { PipesModule } from '../core/pipes/pipes.module';
import { TableModule } from 'primeng/table';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmationService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
@NgModule({
  declarations: [ListComponent],
  schemas: [NO_ERRORS_SCHEMA],
  imports: [
    CommonModule,
    NgScrollbarModule,
    MomentModule,
    CoreModule,
    FormsModule,
    RouterModule,
    PipesModule,
    TableModule,
    ConfirmPopupModule,
    ButtonModule,
    DialogModule
  ],
  exports: [ListComponent],
  providers: [{ provide: PAYMENT_STORAGE_SERVICE, useExisting: SESSION_STORAGE }, ApiService, ConfirmationService]

})
export class PaymentModule { }
