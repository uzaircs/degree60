export class Payment {
  Id?: number;
  amount: number;
  project?: number;
  client?: number;
  received_on?: Date;
  project_id?: number;
  created_at?: Date;
  is_full?: boolean;
  description?: string;
  receipt_no?: string;
}
