import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import { UserStoreService } from '../authorization/user-store.service';
import { StorageService, StorageTranscoder } from 'ngx-webstorage-service';
import { Payment } from './model';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
const STORAGE_KEY = 'db-payments';
export const PAYMENT_STORAGE_SERVICE =
  new InjectionToken<StorageService>(STORAGE_KEY);
@Injectable({
  providedIn: 'root'
})
class PaymentTranscoder implements StorageTranscoder<Payment[]> {
  encode(value: Payment[]): string {
    return JSON.stringify(value);
  }
  decode(value: string): Payment[] {

    return Object.assign(new Array<Payment>(), JSON.parse(value));

  }
}
@Injectable()
export class ApiService {
  public static AUTH_COOKIE = 'Auth-Token';
  baseurl = environment.api;
  private paymentStorage: StorageService<Payment[]>;
  addPayment = (payment: Payment) => {
    return new Promise((resolve, reject) => {
      this.http.post<Payment>(this.baseurl + '/api/sales', payment, { withCredentials: true }).toPromise().then(addedPayment => {
        if (addedPayment.Id) {
          this.http.get<Payment[]>(this.baseurl + '/api/sales', { withCredentials: true }).toPromise().then(response => {
            if (response && Array.isArray(response)) {
              this.paymentStorage.set(STORAGE_KEY, response);
              resolve(addedPayment);
            }
          })
        } else {
          reject('There was an error while adding a payment');
        }
      }).catch((error) => {
        reject(error);
      })
    })

  }
  editPayment = (id, payment) => {
    return new Promise((resolve, reject) => {
      this.http.put(this.baseurl + '/api/sales/' + id, { ...payment }, { withCredentials: true }).toPromise().then(() => {
        this.http.get<Payment[]>(this.baseurl + '/api/sales', { withCredentials: true }).toPromise().then(response => {
          if (response && Array.isArray(response)) {
            this.paymentStorage.set(STORAGE_KEY, response);
            resolve(true);
          }
        })
      })
    })

  }
  deletePayment = id => {
    return new Promise((resolve, reject) => {
      this.http.delete(this.baseurl + '/api/sales/' + id, { withCredentials: true }).toPromise().then(() => {
        resolve(true)
      }).catch((error) => {
        resolve(false);
      })
    })
  }
  getPayments = (month = 0, year = 0): Observable<Payment[]> => {
    const payments = new BehaviorSubject<Payment[]>([]);
    /* payments.next(this.paymentStorage.get(STORAGE_KEY)); */
    this.http.get<Payment[]>(this.baseurl + '/api/sales?month=' + month + '&year=' + year, { withCredentials: true }).toPromise().then(response => {
      if (response && Array.isArray(response) && response.length) {
        payments.next(response);
        this.paymentStorage.set(STORAGE_KEY, payments.value);
      } else {
        payments.error('Failed to retrieve payments')
      }
    })
    return payments;
  }
  constructor(@Inject(PAYMENT_STORAGE_SERVICE) private storage: StorageService, private http: HttpClient) {
    this.paymentStorage = storage.withDefaultTranscoder(new PaymentTranscoder());
  }
}
