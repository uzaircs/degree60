import { Team } from './Team.model';
import { Customer } from './Customer.model';
import { ProjectAttachment } from '../attachments/project-attachment';

export class Project {
  Id: number;
  title: string;
  deadline: Date;
  status: number;
  amount_total: number;
  amount_recieved: number;
  team?: any;
  owner?: number;
  owner_name?: string;
  description: string;
  created_at: string;
  get parsed_date(): Date {
    return new Date(Date.parse(this.created_at));
  }
  Client1: Customer;
  client: number;
  Projects_Attachments: ProjectAttachment[];
}
