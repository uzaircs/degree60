import { Component, OnInit, Input } from '@angular/core';
import { CustomerStoreService } from '../customer-store.service';
import { Customer } from 'src/app/shared/Customer.model';
import { ResizeService } from 'src/app/core/screen.service';
import { Router } from '@angular/router';
export interface UserCustomers {
  name: string;
  id: number;
  customers: Customer[];
}
@Component({
  selector: 'app-customerslist',
  templateUrl: './customerslist.component.html',
  styleUrls: ['./customerslist.component.scss']
})
export class CustomerslistComponent implements OnInit {
  @Input() summary = false;
  mobileView = false;
  user_customers: UserCustomers[];
  constructor(
    private customerStorage: CustomerStoreService,
    private router: Router,
    private resizeService: ResizeService) { }
  customers: Customer[];
  checkScreen() {
    if (this.summary && !this.mobileView) {
      return;
    }
    if (ResizeService.isMobile()) {
      this.summary = true;
      this.mobileView = true;
    } else if (this.mobileView && !ResizeService.isMobile()) {
      this.summary = false;
      this.mobileView = false;
    }

  }
  onClickMe() {
    this.router.navigateByUrl('/app/customers/add')
  }
  ngOnInit() {
    this.customerStorage.fetchCustomers().subscribe($customers => {

      if ($customers && Array.isArray($customers)) {
        this.customers = $customers;
        var owners: UserCustomers[] = $customers
          .map(customer => { return { owner: customer.owner, name: customer.owner_name } })
          .filter(c => c.owner)
          .filter((owner, index, arr) =>
            arr.
              findIndex(o => o.owner === owner.owner) === index).map(userProject => {
                let userProjects = $customers.filter(project => project.owner === userProject.owner);
                return {
                  id: userProject.owner,
                  name: userProject.name,
                  customers: userProjects
                }
              });
        this.user_customers = owners;
      }
    });
    this.checkScreen();
    this.resizeService.onResize$.subscribe(r => {
      this.checkScreen();
    });
  }

}
