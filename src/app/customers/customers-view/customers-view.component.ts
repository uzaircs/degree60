import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerStoreService } from '../customer-store.service';
import { Customer } from 'src/app/shared/Customer.model';
import { ConfirmationService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customers-view',
  templateUrl: './customers-view.component.html',
  styleUrls: ['./customers-view.component.scss']
})
export class CustomersViewComponent implements OnInit {
  id = 0;
  customer: Customer;
  constructor(

    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private customers: CustomerStoreService,
    private toastr: ToastrService, private router: Router
  ) {
    route.paramMap.subscribe(params => this.setCustomer(parseFloat(params.get('id'))));
  }
  deleteCustomer = () => {
    this.customers.deleteCustomer(this.customer.Id).then(r => {
      if (r) {
        this.toastr.success("Customer delete succesfuly");
        this.router.navigateByUrl('/app/customers');
      } else {
        this.toastr.error("Can not delete this customer");
      }
    })

  }
  confirmDelete = event => {
    this.confirmationService.confirm({
      target: event.target,
      message: 'Are you sure you want to delete this customer? All payments and interactions and projects for this customer will permanently deleted.',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.deleteCustomer();
      },
      reject: () => {
        //reject action
      }
    });
  }
  setCustomer(id) {
    this.customers.getCustomers().subscribe(customers => {
      this.customer = customers.find(c => c.Id === id);
    });
    /* .find(c => c.Id === id) */
  }
  ngOnInit() {
  }

}
