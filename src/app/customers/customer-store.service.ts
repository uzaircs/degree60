import { Injectable, InjectionToken, Inject } from '@angular/core';
import { Customer } from '../shared/Customer.model';
import { StorageService, StorageTranscoder } from 'ngx-webstorage-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
const STORAGE_KEY = 'db_customers';
export const CUSTOMER_STORAGE_SERVICE =
  new InjectionToken<StorageService>(STORAGE_KEY);
@Injectable({
  providedIn: 'root'
})
class CustomerTranscoder implements StorageTranscoder<Customer[]> {
  encode(value: Customer[]): string {
    return JSON.stringify(value);
  }
  decode(value: string): Customer[] {
    return Object.assign(new Array<Customer>(), JSON.parse(value));
  }
}
@Injectable()
export class CustomerStoreService {
  baseurl = environment.api;
  customers = new BehaviorSubject<Customer[]>([]);
  /*  customers: Customer[] = []; */
  private customerStorage: StorageService<Customer[]>
  constructor(@Inject(CUSTOMER_STORAGE_SERVICE) private storage: StorageService, private http: HttpClient) {
    this.customerStorage = storage.withDefaultTranscoder(new CustomerTranscoder());
  }
  clear() {
    this.customerStorage.set(STORAGE_KEY, []);
  }
  fetchCustomers(): Observable<Customer[]> {

    const _customers = this.customerStorage.get(STORAGE_KEY);
    if (_customers && Array.isArray(_customers)) {
      this.customers.next(_customers);
    }

    this.http.get<Customer[]>(this.baseurl + '/api/clients', { withCredentials: true }).toPromise().then(FreshCustomers => {
      this.customers.next(FreshCustomers);

      this.customerStorage.set<Customer[]>(STORAGE_KEY, FreshCustomers, new CustomerTranscoder());
    });

    return this.customers.asObservable();
  }
  public editCustomer(customer: Customer): Promise<Customer> {
    return new Promise((resolve, reject) => {
      this.http.put(this.baseurl + '/api/clients/' + customer.Id, customer, { withCredentials: true }).toPromise().then((response) => {
        resolve(customer);
      }).catch((error) => {
        reject(error);
      })
    })

  }
  public addCustomer(customer: Customer): Promise<Customer> {
    return new Promise((resolve, reject) => {
      this.http.post<Customer>(this.baseurl + '/api/clients', customer, { withCredentials: true }).toPromise().then((response) => {
        resolve(response);
        this.getCustomers();
      })
    })

  }
  public deleteCustomer(id: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.delete(this.baseurl + '/api/clients/' + id, { withCredentials: true }).toPromise().then(() => {
        resolve(true)
      }).catch((error) => reject(error))
    })

  }
  public getCustomers() {
    return this.fetchCustomers();
  }
}
