import { Component, OnInit } from '@angular/core';
import { CustomerStoreService } from '../customer-store.service';
import { Customer } from 'src/app/shared/Customer.model';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import countries from '../../core/countries.json'
@Component({
  selector: 'app-customerform',
  templateUrl: './customerform.component.html',
  styleUrls: ['./customerform.component.scss']
})
export class CustomerformComponent implements OnInit {
  customer: Customer;
  isEdit = false;
  ready = false;
  selectedInstitute: string;
  results: string[];
  countries = countries;
  selectedCountry = null;
  constructor(
    private customerStorage: CustomerStoreService,
    private toastr: ToastrService, private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.customer = new Customer();

  }
  search = event => {
    this.results = []
  }
  ngOnInit() {


    this.activatedRoute.paramMap.subscribe(params => {
      let id = params.get('id');
      if (id) {

        this.customerStorage.getCustomers().subscribe(customers => {
          if (customers && Array.isArray(customers) && customers.length > 0) {
            try {
              let customer = customers.filter(c => c.Id == parseInt(id))[0];
              if (customer) {
                this.isEdit = true;
                this.customer = customer;
                this.ready = true;
              }
            } catch (error) {

            }
          }
        })
      } else {
        this.ready = true;
      }
    });
  }
  validate(): boolean {
    if (!this.customer.name) {
      this.toastr.error('Customer name is a required field', 'Please check form');
      return false;
    }
    if (!this.customer.email) {
      this.toastr.error('Customer email is a required field', 'Please check form');
      return false;
    }
    return true;

  }
  save() {
    if (!this.validate()) {
      return false;
    }
    if (this.isEdit) {
      this.customerStorage.editCustomer(this.customer).then(c => {
        this.toastr.success(this.customer.name + ' successfuly updated!', 'Customer Updated!');
        this.router.navigateByUrl('app/customers/view/' + c.Id);
      });
    } else {
      this.customerStorage.addCustomer(this.customer).then(c => {
        this.toastr.success(this.customer.name + ' successfuly added!', 'Customer Added!');
        this.router.navigateByUrl('app/customers/view/' + c.Id);
      });
    }

  }
}
