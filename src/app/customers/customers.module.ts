import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerslistComponent } from './customerslist/customerslist.component';
import { CoreModule } from '../core/core.module';
import { MomentModule } from 'ngx-moment';
import { CustomerformComponent } from './customerform/customerform.component';
import { FormsModule } from '@angular/forms';
import { CUSTOMER_STORAGE_SERVICE, CustomerStoreService } from './customer-store.service';
import { SESSION_STORAGE } from 'ngx-webstorage-service';
import { CustomersViewComponent } from './customers-view/customers-view.component';
import { RouterModule } from '@angular/router';
import { ProjectsModule } from '../projects/projects.module';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { ConfirmationService } from 'primeng/api';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
@NgModule({
  declarations: [CustomerslistComponent, CustomerformComponent, CustomersViewComponent],
  imports: [
    CommonModule,
    CoreModule,
    MomentModule,
    FormsModule,
    RouterModule,
    ProjectsModule,
    NgScrollbarModule,
    DropdownModule,
    AutoCompleteModule,
    TabViewModule,
    ButtonModule,
    ConfirmPopupModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CustomerslistComponent],
  providers: [ CustomerStoreService, ConfirmationService]
})
export class CustomersModule { }
