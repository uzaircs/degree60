import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { CoreModule } from '../core/core.module';
import { MomentModule } from 'ngx-moment';
import { ProjectFormComponent } from './project-form/project-form.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ProjectStoreService, PROJECT_STORAGE_SERVICE } from './project-store.service';
import { SESSION_STORAGE } from 'ngx-webstorage-service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ProjectViewComponent } from './project-view/project-view.component';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { CUSTOMER_STORAGE_SERVICE, CustomerStoreService } from '../customers/customer-store.service';
import { FormsModule } from '@angular/forms';
import { ATTACHMENT_STORAGE_SERVICE, AttachmentStoreService } from '../attachments/attachment-store.service';
import { StatuslabelDirective } from '../directives/statuslabel.directive';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ChatModule } from '../chat/chat.module';
import { ProjectEditComponent } from './projectedit/projectedit.component';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { MenubarModule } from 'primeng/menubar';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { CalendarModule } from 'primeng/calendar';
import { PaymentModule } from '../payment/payment.module';
import { ApiService, PAYMENT_STORAGE_SERVICE } from '../payment/api.service';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { TabViewModule } from 'primeng/tabview';
import { TableComponent } from './table/table.component';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmationService } from 'primeng/api';

@NgModule({
  declarations: [ProjectsListComponent, ProjectFormComponent, ProjectViewComponent, StatuslabelDirective, ProjectEditComponent, TableComponent],
  schemas: [NO_ERRORS_SCHEMA],
  imports: [
    CommonModule,
    CoreModule,
    MomentModule,
    NgxDropzoneModule,
    NgxDatatableModule,
    RouterModule,
    NgScrollbarModule,
    NgSelectModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ChatModule,
    BreadcrumbModule,
    MenubarModule,
    DialogModule,
    NgxSmartModalModule.forRoot(),
    ButtonModule,
    CalendarModule,
    PaymentModule,
    MessageModule,
    MessagesModule,
    TabViewModule,
    ConfirmPopupModule

  ],
  exports: [
    ProjectFormComponent,
    ProjectViewComponent,
    ProjectsListComponent,
    ProjectEditComponent
  ],
  providers: [
    { provide: PROJECT_STORAGE_SERVICE, useExisting: SESSION_STORAGE },
    { provide: CUSTOMER_STORAGE_SERVICE, useExisting: SESSION_STORAGE },
    { provide: ATTACHMENT_STORAGE_SERVICE, useExisting: SESSION_STORAGE },

    ProjectStoreService, CustomerStoreService, AttachmentStoreService, ConfirmationService]
})
export class ProjectsModule { }
