import { Component, OnInit, Input } from '@angular/core';
import { Project } from 'src/app/shared/project.model';
import { ProjectStoreService } from '../project-store.service';
import { ResizeService } from 'src/app/core/screen.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Router } from '@angular/router';
import { LocalDateService } from 'src/app/core/local-date.service';
export interface UserProjects {
  user: string;
  user_id: number;
  projects?: Project[];
}
const MAX_SUMMARY_PROJECTS = 8;
@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent implements OnInit {
  projects: Project[];
  ready = false;
  user_projects: UserProjects[];
  @Input() SummaryView = false;
  @Input() client = -1;
  @Input() month = 0;
  @Input() year = 0;
  months = [];
  years = [];
  mobileView = false;
  constructor(
    private router: Router,
    private localDates: LocalDateService,
    private projectService: ProjectStoreService,
    private resizeService: ResizeService) {

  }
  checkScreen() {
    if (this.SummaryView && !this.mobileView) {
      return;
    }
    if (ResizeService.isMobile()) {
      this.SummaryView = true;
      this.mobileView = true;
    } else if (this.mobileView && !ResizeService.isMobile()) {
      this.SummaryView = false;
      this.mobileView = false;
    }

  }
  onClickMe() {
    this.router.navigateByUrl('/app/projects/add');
  }
  applyFilter() {
    this.load();
  }
  clear() {
    this.month = 0;
    this.year = 0;
    this.load();
  }
  load() {
    this.user_projects = [];
    setTimeout(() => {
      this.projectService.getProjects(this.month, this.year).subscribe(projects => {
        if (projects && Array.isArray(projects) && projects.length > 0) {
          this.ready = true;
          var owners: UserProjects[] = projects
            .map(project => { return { owner: project.owner, name: project.owner_name } })
            .filter((owner, index, arr) =>
              arr.
                findIndex(o => o.owner === owner.owner) === index).map(userProject => {
                  let userProjects = projects.filter(project => project.owner === userProject.owner);
                  return {
                    user_id: userProject.owner,
                    user: userProject.name,
                    projects: userProjects
                  }
                });

          this.user_projects = owners;
        } else {
          setTimeout(() => {
            this.ready = true;
          }, 3000);
        }

        if (this.client >= 0) {
          this.projects = projects.filter(p => p.client === this.client);
        } else {
          this.projects = projects;
        }
        if (this.SummaryView) {
          this.projects = projects;
          /*   this.projects = this.projects.slice(0, MAX_SUMMARY_PROJECTS); */
        }

      });
    }, 250);
  }
  ngOnInit() {
    this.checkScreen();
    this.resizeService.onResize$.subscribe(r => {
      this.checkScreen();
    });
    this.months = this.localDates.getMonths();
    this.years = this.localDates.getYears();
    this.load();

  }

}
