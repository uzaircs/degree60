import { Component, Input, OnInit } from '@angular/core';
import { Interaction } from 'src/app/core/interactions/interaction';
import { Project } from 'src/app/shared/project.model';
import { ProjectStoreService } from '../project-store.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer } from 'src/app/shared/Customer.model';
import { ProjectAttachment } from 'src/app/attachments/project-attachment';
import { ChatStoreService } from 'src/app/chat/chat-store.service';
import { Chat } from 'src/app/chat/chat';
import { environment } from 'src/environments/environment';
import { ConfirmationService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Payment } from 'src/app/payment/model';
import { ApiService } from 'src/app/payment/api.service';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.scss']
})
export class ProjectViewComponent implements OnInit {
  constructor(
    private projectStorage: ProjectStoreService,
    private route: ActivatedRoute,
    private router: Router,
    private paymentStorage: ApiService,
    public ngxSmartModalService: NgxSmartModalService,
    private chatService: ChatStoreService, private toastr: ToastrService,
    private confirmationService: ConfirmationService
  ) {
    route.paramMap.subscribe(params => this.setProject(parseFloat(params.get('id'))));
    this.messages = new Array<Chat>();
    this.items = [
      { icon: 'pi pi-home' },
      { label: 'Projects', url: 'app/projects' },
      { label: 'View Project' },

    ];
    this.menuItems = [

      { label: 'Add Payment', icon: 'pi pi-plus-circle', command: this.toggleModal },
      { label: 'Change Due Date', icon: 'pi pi-calendar' },
      { label: 'Mark Completed', icon: 'pi pi-check-circle', command: this.completed },



    ];

  }
  items: MenuItem[];
  menuItems: MenuItem[];
  modalVisibility = false;
  messages: Chat[];
  project: Project;
  customer: Customer;
  amount_error = false;
  date_error = false;
  @Input() amount: any
  @Input() received_on: any
  openAttachment(attachment: ProjectAttachment) {
    window.open(environment.base + '/Static/' + attachment.VirtualAttachment.attachment);
  }
  removeAttachment = id => {
    let attachment_index = this.project.Projects_Attachments.findIndex(i => i.Id == id);
    if (attachment_index > -1) {
      this.project.Projects_Attachments.splice(attachment_index, 1);
    }
    this.projectStorage.deleteAttachment(id).then(() => {
      this.toastr.success('Attachments successfully deleted');
    })
  }
  confirmDeleteAttachment = (event, id) => {
    this.confirmationService.confirm({
      target: event.target,
      message: 'Are you sure you want to delete this attachment?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.removeAttachment(id);
      },
      reject: () => {
        //reject action
      }
    });
  }
  deleteProject() {
    this.projectStorage.deleteProject(this.project.Id).then(r => {
      if (r) {
        this.toastr.success("Project deleted");
        this.router.navigateByUrl('/app/projects');
      }
    })
  }
  setProject(id: number) {
    this.chatService.getMessages(id).subscribe(messages => {
      this.messages = messages;
    },
      error => this.messages = []);
    this.projectStorage.getProject(id).subscribe(p => {
      if (p) {
        this.project = p;
        this.customer = p.Client1;
        if (this.project.status == 2) {
          this.menuItems.pop();
        }
      }

    });
  }
  confirmDelete = (event) => {

    this.confirmationService.confirm({
      target: event.target,
      message: 'Are you sure you want to delete this project? All payments and interactions will also be deleted for this project.',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.deleteProject();
      },
      reject: () => {
        //reject action
      }
    });
  }
  toggleModal = () => {
    this.modalVisibility = this.ngxSmartModalService.getModal('targetModal').isVisible();
    if (this.modalVisibility) {
      this.ngxSmartModalService.getModal('targetModal').close()
    }
    if (!this.modalVisibility) {
      this.ngxSmartModalService.getModal('targetModal').open()


    }
  }
  completed = () => {
    this.menuItems.pop();
    this.project.status = 2;
    this.toastr.success("Project marked completed", 'Completed');
    this.projectStorage.markCompleted(this.project.Id).then(() => {

    }).catch(() => {
      this.project.status = 1;
    })

  }

  generatePayment = (): Payment => {
    return {
      amount: this.amount,
      project: this.project.Id,
      client: this.project.Client1.Id,
      received_on: this.received_on
    }
  }
  save() {

    if (!this.amount) {
      this.amount_error = true;
    } else {
      this.amount_error = false;
    }
    if (!this.received_on) {
      this.date_error = true;
    } else this.date_error = false;
    if (!this.amount_error && !this.date_error) {
      this.toggleModal();
      let payment = this.generatePayment();
      this.paymentStorage.addPayment(payment).then(() => {
        this.toastr.success("Payment added successfully");

      })
    }
  }
  ngOnInit() {
  }

}
