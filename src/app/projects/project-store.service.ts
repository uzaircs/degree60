import { Injectable, Inject, InjectionToken, OnInit } from '@angular/core';
import { Project } from '../shared/project.model';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService, StorageTranscoder } from 'ngx-webstorage-service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Customer } from '../shared/Customer.model';
import { environment } from 'src/environments/environment';

import moment from 'moment';
const STORAGE_KEY = 'db-projects';
export const PROJECT_STORAGE_SERVICE =
  new InjectionToken<StorageService>(STORAGE_KEY);
@Injectable({
  providedIn: 'root'
})
class ProjectTranscoder implements StorageTranscoder<Project[]> {
  encode(value: Project[]): string {
    return JSON.stringify(value);
  }
  decode(value: string): Project[] {

    return Object.assign(new Array<Project>(), JSON.parse(value));

  }
}

@Injectable()
export class ProjectStoreService implements OnInit {

  baseurl = environment.api;
  projects: Project[] = [];
  private projectStorage: StorageService<Project[]>;
  constructor(@Inject(PROJECT_STORAGE_SERVICE) private storage: StorageService, private http: HttpClient) {
    this.projectStorage = storage.withDefaultTranscoder(new ProjectTranscoder());
  }
  ngOnInit(): void {

  }
  clear() {
    this.projectStorage.set(STORAGE_KEY, []);
  }
  deleteAttachment = id => {
    return new Promise((resolve, reject) => {
      /* api/attachments/5 */
      this.http.delete(this.baseurl + '/api/attachments/' + id, { withCredentials: true }).toPromise().then((response) => {
        this.fetchProjects();
        resolve(true);
      }).catch((error) => {
        reject(error);
      });
    })

  }
  changeDueDate = (id, date) => {
    return new Promise((resolve, reject) => {
      if (id && moment(date).isValid()) {
        this.http.post<Project>(this.baseurl + '/api/v1/changeduedate', { id, date }, { withCredentials: true }).toPromise().then((response) => {
          resolve(true);
          this.fetchProjects();
        }).catch((error) => reject(error));
      } else {
        reject("Date invalid");
      }
    })

  }
  markCompleted = id => {
    return new Promise((resolve, reject) => {
      if (id) {
        this.http.post(this.baseurl + '/api/v1/markcompleted?id=' + id, null, { withCredentials: true }).toPromise().then((response) => {
          resolve(true);
          this.fetchProjects();
        }).catch((error) => reject(error));
      } else {
        reject("id invalid");
      }
    })

  }
  deleteProject(id): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.delete(this.baseurl + '/api/projects/' + id, { withCredentials: true }).toPromise().then((response) => {
        this.fetchProjects();
        resolve(true);
      }).catch((error) => {
        reject(error);
      });
    })

  }
  getProjectDirect = (id: number): Promise<Project> => {
    return new Promise((resolve, reject) => {
      this.http.get<Project>(this.baseurl + '/api/projects/' + id, { withCredentials: true }).toPromise().then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      });
    })

  }
  getProject(id: number): Observable<Project> {
    let projectObservable = new BehaviorSubject<Project>(null);
    try {
      let projects = this.projectStorage.get(STORAGE_KEY).filter(p => p.Id == id);
      if (projects && projects.length == 1) {
        projectObservable.next(projects[0]);
      }
    } catch (error) {

    }
    this.getProjectDirect(id).then(p => {
      projectObservable.next(p);
    })
    return projectObservable;
  }
  private fetchProjects(month = 0, year = 0): Observable<Project[]> {
    // tslint:disable-next-line: variable-name
    const _projects = new BehaviorSubject<Project[]>([]);
    let _oldProjects = this.projectStorage.get(STORAGE_KEY);

    let _storage_projects = [];
    if (_oldProjects && _oldProjects.length) {
      _storage_projects = [..._oldProjects];
    }
    if (_oldProjects && _oldProjects.length && month > 0) {
      _storage_projects = _storage_projects.filter(project => {

        return (moment(project.created_at).month() + 1) == month;
      })
    }
    if (_oldProjects && _oldProjects.length && year > 0) {
      _storage_projects = _storage_projects.filter(project => {
        return moment(project.created_at).year() == year;
      })
    }
    _projects.next(_storage_projects);
    this.http.get<Project[]>(this.baseurl + '/api/projects?month=' + month + '&year=' + year, { withCredentials: true }).subscribe(p => {
      _projects.next(p);

      let arr = new Array<Project>();
      p.forEach(project => {
        const _p = new Project();
        _p.Id = project.Id;
        _p.title = project.title;
        _p.status = project.status;
        _p.deadline = project.deadline;
        _p.created_at = project.created_at;
        _p.Client1 = new Customer();
        _p.Client1.name = project.Client1.name;
        _p.amount_recieved = project.amount_recieved;
        _p.amount_total = project.amount_total;
        arr.push(_p);
      });
      if (month == 0 && year == 0) {
        this.projectStorage.clear();
        this.projectStorage.set(STORAGE_KEY, arr);
      }
    });
    return _projects.asObservable();
  }
  public editProject(project: Project): Observable<Project> {
    return this.http.put<Project>(this.baseurl + '/api/projects/' + project.Id, {
      title: project.title,
      amount_recieved: project.amount_recieved,
      amount_total: project.amount_total,
      deadline: project.deadline,
      description: project.description,
      status: project.status,
      team: project.team.Id,
      client: project.client

    }, { withCredentials: true });
  }
  public addProject(project: Project): Promise<Project> {

    this.getProjects().subscribe(projects => {
      if (!projects) { projects = new Array<Project>(); }
      projects.push(project);
      this.projectStorage.set(STORAGE_KEY, projects);
    });
    if (project.team && project.team.Id) {
      project.team = project.team.Id;
    } else {
      project.team = null;
    }
    return new Promise((resolve, reject) => {
      this.http.post<Project>(this.baseurl + '/api/projects', project, { withCredentials: true }).toPromise().then((response) => {
        resolve(response);
        this.fetchProjects();
      }).catch((error) => reject(error));
    })

  }
  public getProjects(month = 0, year = 0) {
    return this.fetchProjects(month, year);
  }
  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    /*   return throwError(errorMessage); */
  }
}
