import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ProjectStoreService } from '../project-store.service';
import { Project } from 'src/app/shared/project.model';
import { Team } from 'src/app/shared/Team.model';
import { CustomerStoreService } from 'src/app/customers/customer-store.service';
import { SelectItem } from 'src/app/shared/SelectItem';
import { ToastrService } from 'ngx-toastr';
import { AttachmentStoreService } from 'src/app/attachments/attachment-store.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { TeamStoreService } from 'src/app/team/team-store.service';
import { ApiService } from 'src/app/payment/api.service';


@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss']
})
export class ProjectFormComponent implements OnInit, OnChanges {

  customers: SelectItem[];
  selectedCustomer: SelectItem;
  project: Project;
  isBusy = false;
  teams: SelectItem[];
  statusList: SelectItem[];
  @Input() edit: Project;
  amount_description: any;
  receipt_no: any;
  files: File[];


  constructor(
    private projectStorage: ProjectStoreService, private customerStorage: CustomerStoreService,
    private toastr: ToastrService, private attachmentStore: AttachmentStoreService,
    private router: Router,
    private teamStore: TeamStoreService,
    private paymentStorage: ApiService
  ) {
    customerStorage.getCustomers().subscribe(customers => this.customers = customers.map(c => new SelectItem(c.Id, c.name)));
    this.project = new Project();
    this.getTeams();
    this.files = new Array<File>();

  }
  onFilesAdded(files: File[]) {
    files.forEach(file => this.files.push(file));
  }
  saveAttachments(project: Project): Promise<void> {
    return new Promise((resolve, reject) => {
      this.toastr.info('Uploading attachments please wait...', 'Please wait');
      const d = new FormData();
      this.files.forEach(file => {
        d.append('file', file, file.name);
      });
      this.attachmentStore.AddAttachments(d, project).add(x => {
        resolve();
      });
    })

  }
  getTeams() {
    this.teamStore.getTeams().subscribe(t => {
      if (t && Array.isArray(t)) {
        this.teams = t.map<SelectItem>(res => {
          return {
            id: res.Id,
            name: res.name
          } as SelectItem;
        });
      }

    });
  }
  validate(): boolean {
    if (!this.project.title) {
      this.toastr.error('Title is a required field, Please add a project title', 'Please check');
      return false;
    }
    if (!this.edit && isNaN(this.project.amount_recieved)) {
      this.toastr.error('The amount recieved you entered is invalid', 'Please check');
      return false;
    }
    if (!this.project.amount_total || isNaN(this.project.amount_total)) {
      this.toastr.error('The amount total you entered is invalid', 'Please check');
      return false;
    }
    if (!this.project.deadline || !moment(this.project.deadline).isValid()) {
      this.toastr.error('Please enter a valid deadline for this project', 'Please check');
      return false;
    }
    return true;
  }
  save() {
    if (!this.validate()) { return; }
    const waitToast = this.toastr.info('Saving project...', 'Please wait');
    this.isBusy = true;
    if (!this.edit) {
      this.projectStorage.addProject(this.project).then(p => {
        if (this.files.length) {
          this.saveAttachments(p).then(() => {
            waitToast.toastRef.close();
            if (this.project.amount_recieved && this.project.amount_recieved > 0) {
              this.paymentStorage.addPayment({
                amount: this.project.amount_recieved,
                is_full: this.project.amount_recieved == this.project.amount_total,
                client: this.project.client,
                project: p.Id,
                description: this.amount_description,
                receipt_no: this.receipt_no


              }).then((result) => {
                this.toastr.success('Project succesfuly added', 'Project Added');
                this.router.navigateByUrl('/app/projects/view/' + p.Id);
              })
            } else {
              this.toastr.success('Project succesfuly added', 'Project Added');
              this.router.navigateByUrl('/app/projects/view/' + p.Id);
            }
          });
        } else {
          waitToast.toastRef.close();
          if (this.project.amount_recieved && this.project.amount_recieved > 0) {
            this.paymentStorage.addPayment({
              amount: this.project.amount_recieved,
              is_full: this.project.amount_recieved == this.project.amount_total,
              client: this.project.client,
              project: p.Id,
              description: this.amount_description,
              receipt_no: this.receipt_no

            }).then((result) => {
              this.toastr.success('Project succesfuly added', 'Project Added');
              this.router.navigateByUrl('/app/projects/view/' + p.Id);
            })
          } else {
            this.toastr.success('Project succesfuly added', 'Project Added');
            this.router.navigateByUrl('/app/projects/view/' + p.Id);
          }
        }


      });
    } else {

      this.projectStorage.editProject(this.project).subscribe(p => {
        if (this.files.length) {
          this.saveAttachments(p);
        } else {
          this.toastr.success('Project succesfuly edited', 'Project Edited');
          this.router.navigateByUrl('/app/projects/view/' + p.Id);

        }
      });
    }
  }
  ngOnInit() {
    // tslint:disable-next-line: max-line-length
    const dzIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="58.481" height="58.466" viewBox="0 0 58.481 58.466"><g id="cloud" transform="translate(0 0)"><path id="Path_7" data-name="Path 7" d="M64.531,140.359a10.716,10.716,0,0,0-3.139-7.583,19.862,19.862,0,0,0-4.6-2.717,19.561,19.561,0,0,0-2.98-.421,10.6,10.6,0,0,0-3.908.741A13.572,13.572,0,0,0,44.7,119.891H42.114v25.342h-11.7V119.891H27.835a13.645,13.645,0,0,0-4.99,8.236,11.164,11.164,0,0,0-3.148-.438,11.7,11.7,0,0,0,0,23.392H53.81a10.716,10.716,0,0,0,10.721-10.721Zm0,0" transform="translate(-7.026 -105.284)" fill="#2e62ec" opacity="0.33"/><path id="Path_8" data-name="Path 8" d="M30.215,50.808V46.771h16.57a11.7,11.7,0,1,0-3.013-23,14.479,14.479,0,0,0-3.656-8.187h1.8a.975.975,0,0,0,.714-1.639L29.955.3a1.007,1.007,0,0,0-1.429,0L15.855,13.943a.975.975,0,0,0,.714,1.639h1.786a14.58,14.58,0,0,0-3.272,6.079,12.477,12.477,0,0,0-2.412-.231,12.671,12.671,0,0,0,0,25.342H28.266v4.037a3.9,3.9,0,0,0-2.786,2.786H0v1.949H25.48a3.88,3.88,0,0,0,7.52,0h25.48V53.594H33a3.9,3.9,0,0,0-2.786-2.786ZM29.24,2.393,39.676,13.632H35.088a.975.975,0,0,0-.975.975V38.973H24.367V14.606a.975.975,0,0,0-.975-.975H18.8ZM1.949,34.1A10.734,10.734,0,0,1,12.671,23.379a10.157,10.157,0,0,1,2.874.4.974.974,0,0,0,1.233-.758,12.641,12.641,0,0,1,4.393-7.439h1.247V39.948a.975.975,0,0,0,.975.975h11.7a.975.975,0,0,0,.975-.975V15.581h1.243a12.564,12.564,0,0,1,4.6,9.528A.975.975,0,0,0,43.234,26a9.618,9.618,0,0,1,3.551-.672,9.747,9.747,0,0,1,0,19.494H12.671A10.735,10.735,0,0,1,1.949,34.1ZM29.24,56.518a1.949,1.949,0,1,1,1.949-1.949A1.949,1.949,0,0,1,29.24,56.518Zm0,0" transform="translate(0 0)" fill="#2e62ec" opacity="0.87"/></g></svg>`;
    const iconElement = document.createElement('div');
    iconElement.innerHTML = dzIcon;
    const dz = document.getElementsByClassName('dropzone')[0];
    dz.insertBefore(iconElement, dz.childNodes[0]);
    this.statusList = new Array<SelectItem>();
    this.statusList.push({ id: 0, name: 'Pending' });
    this.statusList.push({ id: 1, name: 'Active' });
    this.statusList.push({ id: 2, name: 'Completed' });
    if (this.edit) {
      this.project.status = this.edit.status;
      if (!this.project.team) {
        this.project.team = {
          Id: null,
        }
      }
      console.log(this.project);
    } else {
      this.project.status = 0;
      this.project.team = {
        Id: null
      }
    }

  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.edit.currentValue) {
      this.project = this.edit;
      this.selectedCustomer = new SelectItem(this.project.Client1.Id, this.project.Client1.name);
      this.project.client = this.selectedCustomer.id;

    }
  }

}
