import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() projects;
  @Input() hideUser = false;
  @Input() SummaryView = false;
  constructor() { }

  ngOnInit(): void {
  }

}
