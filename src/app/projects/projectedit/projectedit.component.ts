import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/shared/project.model';
import { ProjectStoreService } from '../project-store.service';

@Component({
  selector: 'app-projectedit',
  templateUrl: './projectedit.component.html',
  styleUrls: ['./projectedit.component.scss']
})
export class ProjectEditComponent implements OnInit {
  project: Project;
  busy = true;
  constructor(private route: ActivatedRoute, private projectStore: ProjectStoreService, private router: Router) {
    route.paramMap.subscribe(p => {
      this.setProject(parseFloat(p.get('id')));
    });
  }
  setProject(Id: number) {
    if (Id) {
      this.projectStore.getProjectDirect(Id).then(p => {
        if (p) {

          this.project = p;
          this.busy = false;
        } else {

        }
      });
    }
  }
  ngOnInit() {
  }

}
