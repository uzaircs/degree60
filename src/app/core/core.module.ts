import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CardComponent } from './card/card.component';
import { RouterModule } from '@angular/router';
import { HeroCardComponent } from './hero-card/hero-card.component';
import { InteractionsComponent } from './interactions/interactions.component';
import { FormsModule } from '@angular/forms';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { MomentModule } from 'ngx-moment';
import { SalesTargetComponent } from './sales-target/sales-target.component';
import { PipesModule } from './pipes/pipes.module';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { LoadingComponent } from './loading/loading.component';
import { EmptyComponent } from './empty/empty.component';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  declarations: [HeaderComponent, SidebarComponent, CardComponent, HeroCardComponent, InteractionsComponent, SalesTargetComponent, LoadingComponent, EmptyComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgScrollbarModule,
    MomentModule,
    PipesModule,
    ProgressSpinnerModule,
    TooltipModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    HeaderComponent,
    SidebarComponent,
    CardComponent,
    HeroCardComponent,
    InteractionsComponent,
    SalesTargetComponent,
    LoadingComponent,
    EmptyComponent

  ]

})
export class CoreModule { }
