export class Interaction {
    Name: string;
    Body: string;
    Created: Date;
    isOwner: boolean;
}
