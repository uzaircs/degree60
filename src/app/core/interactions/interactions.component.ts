import { Component, OnInit, Input, ViewChild, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { Interaction } from './interaction';
import { Chat } from 'src/app/chat/chat';
import { Project } from 'src/app/shared/project.model';
import { ChatStoreService } from 'src/app/chat/chat-store.service';
import { UserStoreService } from 'src/app/authorization/user-store.service';
import { NgScrollbar } from 'ngx-scrollbar';

@Component({
  selector: 'app-interactions',
  templateUrl: './interactions.component.html',
  styleUrls: ['./interactions.component.scss']
})
export class InteractionsComponent implements OnInit, AfterViewInit, OnChanges {
  account_id: number;
  ready = false;
  @Input() messages: Chat[];
  @Input() project: Project;
  @ViewChild(NgScrollbar) chatScroll: NgScrollbar;
  message: Chat;
  constructor(private chatService: ChatStoreService, private userStore: UserStoreService) {

  }
  send() {

    this.message.account = this.account_id;
    this.chatService.AddMessage(this.message).subscribe(m => {
      this.initChat();
      m.message.replace('\n', '<br>');
      this.messages.push(m);
      setTimeout(() => {
        this.autoScroll();
      }, 1000);
    });

  }
  ngOnChanges(changes: SimpleChanges): void {
    setTimeout(() => {
      this.autoScroll();
    }, 1000);
  }

  initChat() {
    this.message = new Chat();
    this.message.project = this.project.Id;
    this.message.self = true;
  }
  ngOnInit() {
    this.userStore.RequestUser().subscribe(u => {
      if (u && u.Id) {
        this.account_id = u.Id;
        this.ready = true;
      }
    });
    this.initChat();

  }
  autoScroll(): void {
    this.chatScroll.scrollTo({ bottom: 0, duration: 200 });
  }
  ngAfterViewInit(): void {
    this.autoScroll();
  }

}
