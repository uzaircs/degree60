import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Sale } from 'src/app/sales/sale';
import * as moment from 'moment';
@Component({
  selector: 'app-sales-target',
  templateUrl: './sales-target.component.html',
  styleUrls: ['./sales-target.component.scss']
})
export class SalesTargetComponent implements OnInit, OnChanges {

  progress: number;
  mString: string;
  @Input() sale: Sale;
  constructor() { }

  ngOnInit() {
    this.mString = moment(new Date(this.sale.TargetYear, this.sale.TargetMonth - 1)).format('MMM, YYYY');
    if (this.sale.amount_completed) {
      this.progress = ((this.sale.amount_completed / this.sale.amount) * 100);
    } else {
      this.progress = 0;
    }

  }

  ngOnChanges(changes: SimpleChanges): void {
    this.sale = changes.sale.currentValue;
    this.ngOnInit();

  }

}
