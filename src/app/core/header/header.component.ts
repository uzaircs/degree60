import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/authorization/auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() sidebarToggle = new EventEmitter();
  sidebarOpened = false;
  constructor(private LoginService: AuthService, private router: Router) { }
  logout() {

    this.LoginService.DeAuthorize().subscribe(x => {
      this.router.navigateByUrl('/login').then(complete => {
        location.reload();
      });

    });
  }
  toggleSidebar() {
    this.sidebarOpened = !this.sidebarOpened;
    this.sidebarToggle.emit(this.sidebarOpened);
  }
  ngOnInit() {
  }

}
