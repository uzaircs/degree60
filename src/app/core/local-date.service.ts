import { Injectable } from '@angular/core';
import { SelectItem } from '../shared/SelectItem';

@Injectable({
  providedIn: 'root'
})
export class LocalDateService {
  MONTHS: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
    'August', 'September', 'October', 'November', 'December'];
  constructor() { }
  getMonths(): SelectItem[] {
    return this.MONTHS.map<SelectItem>((s, i) => {
      return new SelectItem(i + 1, s);
    });
  }
  getYears(): SelectItem[] {
    const items = new Array<SelectItem>();
    for (let index = 0; index < 10; index++) {
      const year = new Date();
      year.setFullYear(year.getFullYear() + (index - 5));
      items.push(new SelectItem(year.getFullYear(), year.getFullYear().toString()));
    }
    return items;
  }

}
