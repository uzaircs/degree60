import { Component, OnInit, Input, ViewEncapsulation, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from "@angular/router";
@Component({
  selector: 'app-card',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {

  clickMessage = '';
  constructor(private router: Router) { }
  @Input() styleClass = '';
  @Input() heading = '';
  @Input() footer = '';
  @Input() routeIcon = 'plus';
  @Input() routeLabel = 'Add';
  @Input() subheading = '';
  @Input() route = '';
  @Input() padding: string;
  ngOnInit() {
    if (!this.padding) {
      this.padding = '30px';
    }
  }
  onClickMe() {
    this.router.navigateByUrl(this.route);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.padding && changes.padding.currentValue !== changes.padding.previousValue) {
      this.padding = changes.padding.currentValue;
    }
  }

}
