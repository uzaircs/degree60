import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currency'
})
export class CurrencyPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if (value && !isNaN(+value)) {
      return '$' + (+value).toFixed(0);
    } else return '-'
  }

}
