import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyPipe } from './currency.pipe';
import { EasydatePipe } from './easydate.pipe';


@NgModule({
  declarations: [CurrencyPipe, EasydatePipe],
  imports: [
    CommonModule
  ],
  exports: [CurrencyPipe, EasydatePipe]
})
export class PipesModule { }
