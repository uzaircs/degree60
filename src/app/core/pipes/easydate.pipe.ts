import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'easydate'
})
export class EasydatePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if (value) {
      if (moment(value).isValid()) {
        return moment(value).format('MMM Do YYYY');
      } else return '-'
    } else return '-';
  }

}
