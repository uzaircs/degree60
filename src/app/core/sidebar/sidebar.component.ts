import { Component, OnInit, Input } from '@angular/core';
import { SidebarItem } from './sidebar-item';
import { MenuService } from './menu-service.service';
import { UserStoreService } from 'src/app/authorization/user-store.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuItems: SidebarItem[];
  @Input() opened;
  constructor(private menuService: MenuService, private authService: UserStoreService) { }

  ngOnInit() {
    this.authService.RequestUser().subscribe(user => {
      if (user.role) {
        this.menuItems = this.menuService.getItems(+user.role);

      }
    });
    document.getElementsByTagName('body')[0].addEventListener('click', e => {
      if (!(e.target as HTMLSpanElement).classList.contains('mdi-menu')) {
        this.opened = false;
      }
    });

  }

}
