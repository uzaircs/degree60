import { Injectable } from '@angular/core';
import { SidebarItem } from './sidebar-item';


@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor() { }
  getItems(role: number): SidebarItem[] {
    const items = new Array<SidebarItem>();

    items.push({
      icon: 'mdi-monitor',
      title: 'Dashboard',
      link: '/app/home'
    });

    items.push({
      icon: 'mdi-folder-outline',
      title: 'Projects',
      link: '/app/projects'
    });
    if (role === 1 || role === 2) {

      items.push({
        icon: 'mdi-account-outline',
        title: 'Customers',
        link: '/app/customers'
      });
      items.push({
        icon: 'mdi-bullseye-arrow',
        title: 'Targets',
        link: '/app/targets'
      });
      items.push({
        icon: 'mdi-cash-usd',
        title: 'Sales',
        link: '/app/sales'
      })

    }
    if (role === 1) {
      items.push({
        icon: 'mdi-account',
        title: 'Accounts',
        link: '/app/accounts'
      });
    }
    return items;
  }
}
