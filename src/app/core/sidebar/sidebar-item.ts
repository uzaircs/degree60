import { RouterLink } from '@angular/router';

export class SidebarItem {
    title: string;
    link: string;
    icon: string;
}
