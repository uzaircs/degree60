import { TestBed } from '@angular/core/testing';

import { LocalDateService } from './local-date.service';

describe('LocalDateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalDateService = TestBed.get(LocalDateService);
    expect(service).toBeTruthy();
  });
});
