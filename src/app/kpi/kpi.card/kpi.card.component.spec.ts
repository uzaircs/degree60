import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Kpi.CardComponent } from './kpi.card.component';

describe('Kpi.CardComponent', () => {
  let component: Kpi.CardComponent;
  let fixture: ComponentFixture<Kpi.CardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kpi.CardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Kpi.CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
