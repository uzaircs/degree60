import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-kpi-card',
  templateUrl: './kpi.card.component.html',
  styleUrls: ['./kpi.card.component.scss']
})
export class KpiCardComponent implements OnInit {
  @Input() heading = "";
  @Input() value = "";
  @Input() icon = "";
  constructor() { }

  ngOnInit(): void {
  }

}
