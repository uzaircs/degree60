import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class V1Service {
  baseurl = environment.api;
  constructor(private http: HttpClient) { }
  getKPI(): Promise<KPI> {
    return new Promise((resolve, reject) => {
      this.http.get<KPI>(this.baseurl + '/api/v1/kpi', { withCredentials: true }).toPromise().then(result => {
        resolve(result);
      }).catch(err => reject(err));
    })

  }
}
export class KPI {
  projects?: number;
  completed_projects?: number;
  total_sales_amount?: number;
  total_clients?: number;
  projects_in_progress?: number;
  outstanding_amount?: number;
}
