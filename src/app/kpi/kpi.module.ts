import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KpiCardComponent } from './kpi.card/kpi.card.component';



@NgModule({
  declarations: [KpiCardComponent],
  imports: [
    CommonModule
  ],
  exports: [KpiCardComponent]
})
export class KpiModule { }
