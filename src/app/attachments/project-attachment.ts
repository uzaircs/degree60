export class ProjectAttachment {
    Id: number;
    attachment: number;
    project: number;
    VirtualAttachment: VirtualFile;
}
export class VirtualFile {
    Id: number;
    attachment: string;
    file_name: string;
}
