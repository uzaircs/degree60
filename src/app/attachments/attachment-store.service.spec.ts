import { TestBed } from '@angular/core/testing';

import { AttachmentStoreService } from './attachment-store.service';

describe('AttachmentStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AttachmentStoreService = TestBed.get(AttachmentStoreService);
    expect(service).toBeTruthy();
  });
});
