import { Injectable, Inject, InjectionToken } from '@angular/core';
import { Attachment } from './attachment';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService, StorageTranscoder } from 'ngx-webstorage-service';
import { Observable, Subscription } from 'rxjs';
import { Project } from '../shared/project.model';
import { ProjectAttachment } from './project-attachment';
import { environment } from 'src/environments/environment';
const STORAGE_KEY = 'db-attachments';
export const ATTACHMENT_STORAGE_SERVICE =
  new InjectionToken<StorageService>(STORAGE_KEY);
@Injectable({
  providedIn: 'root'
})
class AttachmentTranscoder implements StorageTranscoder<Attachment[]> {
  encode(value: Attachment[]): string {
    return JSON.stringify(value);
  }
  decode(value: string): Attachment[] {

    return Object.assign(new Array<Attachment>(), JSON.parse(value));

  }
}

@Injectable()
export class AttachmentStoreService {
  baseurl = environment.api;
  Attachments: Attachment[] = [];
  private AttachmentStorage: StorageService<Attachment[]>;
  constructor(@Inject(ATTACHMENT_STORAGE_SERVICE) private storage: StorageService, private http: HttpClient) {
    this.AttachmentStorage = storage.withDefaultTranscoder(new AttachmentTranscoder());
  }

  private fetchAttachments(): Observable<Attachment[]> {
    return this.http.get<Attachment[]>(this.baseurl + '/api/attachments', { withCredentials: true });
    const AttachmentS = this.AttachmentStorage.get<Attachment[]>(STORAGE_KEY, new AttachmentTranscoder());
    return new Observable(Attachments => Attachments.next(AttachmentS));
  }
  private addAttachment(attachment: FormData): Observable<Attachment[]> {

    /* this.getAttachments().subscribe(Attachments => {
      if (!Attachments) { Attachments = new Array<Attachment>(); }
      Attachments.push(attachment);
      this.AttachmentStorage.set(STORAGE_KEY, Attachments);
    }); */
    return this.http.post<Attachment[]>(this.baseurl + '/api/attachments', attachment, { withCredentials: true });
  }
  private BindAttachment(attachment: Attachment[], project: Project): Observable<ProjectAttachment> {
    const attachments = [];
    attachment.forEach(a => {
      const unbound = new ProjectAttachment();
      unbound.attachment = a.Id;
      unbound.project = project.Id;
      attachments.push(unbound);
    });

    return this.http.post<ProjectAttachment>(this.baseurl + '/api/projectattachment', attachments, { withCredentials: true });
  }
  public AddAttachments(attachments: FormData, project: Project): Subscription {
    return this.addAttachment(attachments).subscribe(response => {
      this.BindAttachment(response, project).subscribe();
    });
  }
  public getAttachments() {
    return this.fetchAttachments();
  }
  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    /*   return throwError(errorMessage); */
  }
}
