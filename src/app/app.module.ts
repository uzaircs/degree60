import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { CustomersModule } from './customers/customers.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProjectsModule } from './projects/projects.module';
import { MomentModule } from 'ngx-moment';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SalesModule } from './sales/sales.module';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { AccountsModule } from './accounts/accounts.module';
import { HomeModule } from './dashboard/home.module';
import { ToastrModule } from 'ngx-toastr';
import { StatuslabelDirective } from './directives/statuslabel.directive';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { USER_STORAGE_SERVICE, UserStoreService } from './authorization/user-store.service';
import { SESSION_STORAGE } from 'ngx-webstorage-service';
import { BreadcrumbModule } from 'primeng/breadcrumb';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    ToastrModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    CustomersModule,
    BrowserAnimationsModule,
    ProjectsModule,
    MomentModule,
    NgxDropzoneModule,
    SalesModule,
    NgScrollbarModule,
    AccountsModule,
    HttpClientModule,
    HomeModule,
    FormsModule,
    BreadcrumbModule,


  ],

  providers: [CookieService,
    {
      provide: USER_STORAGE_SERVICE,
      useExisting: SESSION_STORAGE
    }, UserStoreService],
  bootstrap: [AppComponent]
})
export class AppModule {

}
