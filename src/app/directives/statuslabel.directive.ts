import { Directive, ElementRef, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appStatuslabel]'
})
export class StatuslabelDirective implements OnInit, OnChanges {

  @Input() appStatuslabel: number;

  constructor(private el: ElementRef) { }
  ngOnInit(): void {
    this.el.nativeElement.innerHTML = this.parseLabel(this.appStatuslabel).outerHTML;


  }
  ngOnChanges(changes: SimpleChanges): void {
    this.el.nativeElement.innerHTML = this.parseLabel(this.appStatuslabel).outerHTML;
  }
  parseLabel(status: number): HTMLSpanElement {
    const statusLabel = document.createElement('span');
    let label = 'Pending';
    let css = 'label-pending';
    switch (status) {
      case 0:
        label = 'Pending';
        css = 'label-pending';
        break;
      case 1:
        label = 'Active';
        css = 'label-active';
        break;
      case 2:
        label = 'Completed';
        css = 'label-completed';
        break;
      default:
        break;

    }
    statusLabel.classList.add('label');
    statusLabel.classList.add(css);
    statusLabel.innerText = label;
    return statusLabel;

  }
}
