import { Component, OnInit } from '@angular/core';
import { UserStoreService } from '../authorization/user-store.service';
import { User } from '../authorization/user';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  sidebarOpened = false;
  user: User;
  constructor(private userStore: UserStoreService) { }

  ngOnInit() {
    if (this.userStore.getUser() && this.userStore.getUser().account) {
      this.user = this.userStore.getUser();
    } else {
      this.userStore.RequestUser().subscribe(user => {
        this.user = new User();
        this.user.account = user;
        this.user.token = user.token;
      });
    }
  }
  toggleSidebar() {
    this.sidebarOpened = !this.sidebarOpened;

  }

}
