import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerslistComponent } from './customers/customerslist/customerslist.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProjectsListComponent } from './projects/projects-list/projects-list.component';
import { CustomerformComponent } from './customers/customerform/customerform.component';
import { ProjectFormComponent } from './projects/project-form/project-form.component';
import { ProjectViewComponent } from './projects/project-view/project-view.component';
import { SalesListComponent } from './sales/sales-list/sales-list.component';
import { CustomersViewComponent } from './customers/customers-view/customers-view.component';
import { AccountsListComponent } from './accounts/accounts-list/accounts-list.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { RoleGuardService as RoleGuard } from './authorization/role-guard.service';
import { ProjectEditComponent } from './projects/projectedit/projectedit.component';
import { AccountFormComponent } from './accounts/account-form/account-form.component';
import { AccountViewComponent } from './accounts/account-view/account-view.component';
import { TableviewComponent } from './sales/tableview/tableview.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'app/home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'app',
    component: MainComponent,
    canActivate: [RoleGuard],
    pathMatch: 'prefix',
    children: [
      { path: 'customers', component: CustomerslistComponent },
      { path: 'customers/view/:id', component: CustomersViewComponent },
      { path: 'home', component: DashboardComponent },
      { path: 'projects', component: ProjectsListComponent },
      { path: 'accounts', component: AccountsListComponent },
      { path: 'accounts/view/:id', component: AccountViewComponent },
      { path: 'accounts/add', component: AccountFormComponent },
      { path: 'targets', component: SalesListComponent },
      { path: 'sales', component: TableviewComponent },
      { path: 'projects/add', component: ProjectFormComponent },
      { path: 'projects/edit/:id', component: ProjectEditComponent },
      { path: 'projects/view/:id', component: ProjectViewComponent },
      { path: 'customers/add', component: CustomerformComponent },
      { path: 'customers/edit/:id', component: CustomerformComponent },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
