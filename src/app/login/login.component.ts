import { Component, OnInit } from '@angular/core';
import { AuthService } from '../authorization/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { UserStoreService } from '../authorization/user-store.service';
import { environment } from 'src/environments/environment';
import moment from 'moment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public auth = {
    user: '',
    pass: ''
  };

  constructor(
    private LoginService: AuthService, private router: Router, private toastr: ToastrService,
    private cookieService: CookieService, private userStorage: UserStoreService) { }
  login() {
    this.LoginService.Authorize(this.auth.user, this.auth.pass).subscribe(user => {
      let expiry = moment().add(30,'days').toDate();
      this.cookieService.set(AuthService.AUTH_COOKIE, user.token, expiry, ',', environment.host);
      this.userStorage.setUser(user);
      this.router.navigateByUrl('/app/home');
      document.getElementsByTagName('body')[0].style.background = '';

    }, error => {
      this.toastr.error('Invalid credentials please try again', 'Invalid Credentials');
    });
  }
  ngOnInit() {

    this.userStorage.RequestUser().subscribe(user => {
      if (user.Id) {
        this.router.navigateByUrl('/app/home');
      }

    }, error => {
      document.getElementsByTagName('body')[0].style.background = 'white';
    });
  }

}
