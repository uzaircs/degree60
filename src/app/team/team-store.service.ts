import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Team } from './team';

@Injectable({
  providedIn: 'root'
})
export class TeamStoreService {

  constructor(private http: HttpClient) { }
  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(environment.api + '/api/team', { withCredentials: true });
  }
}
