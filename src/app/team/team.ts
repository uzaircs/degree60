export class Team {
    Id: number;
    name: string;
    email: string;
    // tslint:disable-next-line: variable-name
    created_at: Date;
    phone: string;
}
