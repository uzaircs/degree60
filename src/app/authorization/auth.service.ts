import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { UserStoreService } from './user-store.service';
import { Account } from '../accounts/account';
import { environment } from 'src/environments/environment';
import { ProjectStoreService } from '../projects/project-store.service';
import { CustomerStoreService } from '../customers/customer-store.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private userStorage: UserStoreService, private projectStorage: ProjectStoreService, private customerStorage: CustomerStoreService
  ) { }
  public static AUTH_COOKIE = 'Auth-Token';
  baseurl = environment.api;
  getUser(): User {
    return this.userStorage.getUser();
  }
  DeAuthorize() {
    this.userStorage.clearUser();
    this.cookieService.deleteAll();
    this.projectStorage.clear();
    this.customerStorage.clear();
    return this.http.get(this.baseurl + '/api/auth/-1', { withCredentials: true });
  }
  Authorize(username: string, password: string): Observable<User> {
    return this.http.post<User>(this.baseurl + '/api/auth', { username, password });
  }
  isAuthenticated(): Observable<Account> {
    return this.http.get<Account>(this.baseurl + '/api/auth', { withCredentials: true });


  }
}
