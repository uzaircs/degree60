import { Injectable } from '@angular/core';

import { catchError, map } from 'rxjs/operators';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';
import { Observable, BehaviorSubject, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router, private toastr: ToastrService) { }
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.auth.isAuthenticated().pipe(map(e => {
      if (e.Id) {
        return true;
      } else {
        return false;
      }

    }), catchError(err => {
      this.router.navigateByUrl('/login');
      return of(false);
    }));
  }
}
