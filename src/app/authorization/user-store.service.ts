import { Injectable, Inject, InjectionToken } from '@angular/core';

import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService, StorageTranscoder, StorageTranscoders } from 'ngx-webstorage-service';
import { Observable } from 'rxjs';
import { User, Account } from './user';
import { environment } from 'src/environments/environment';
const STORAGE_KEY = 'db-user';
export const USER_STORAGE_SERVICE =
  new InjectionToken<StorageService>(STORAGE_KEY);
@Injectable({
  providedIn: 'root'
})
class UserTranscoder implements StorageTranscoder<User> {
  encode(value: User): string {

    return JSON.stringify(value);
  }
  decode(value: string): User {

    return JSON.parse(value);

  }
}

@Injectable({ providedIn: 'root' })
export class UserStoreService {
  baseurl = environment.api;
  users: User[] = [];
  private userStorage: StorageService<User>;
  constructor(@Inject(USER_STORAGE_SERVICE) private storage: StorageService, private http: HttpClient) {
    this.userStorage = storage.withDefaultTranscoder(new UserTranscoder());
  }
  setUser(user: User) {
    window.localStorage.setItem(STORAGE_KEY, JSON.stringify(user));
    /*  this.userStorage.set(STORAGE_KEY, user); */

  }
  clearUser() {
    window.localStorage.removeItem(STORAGE_KEY);
  }
  RequestUser(): Observable<Account> {
    return this.http.get<Account>(this.baseurl + '/api/auth', {
      withCredentials: true
    });
  }
  getUser(): User {
    try {
      let user = JSON.parse(window.localStorage.getItem(STORAGE_KEY));
      return user;
    } catch (error) {

    }
    return null;
  }




}
