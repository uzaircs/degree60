import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { USER_STORAGE_SERVICE, UserStoreService } from './user-store.service';
import { SESSION_STORAGE } from 'ngx-webstorage-service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: USER_STORAGE_SERVICE,
      useExisting: SESSION_STORAGE
    }, UserStoreService
  ]
})
export class AuthorizationModule { }
