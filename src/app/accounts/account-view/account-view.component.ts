import { Component, OnInit } from '@angular/core';
import { Account } from '../account';
import { AccountStoreService } from '../account-store.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatStoreService } from 'src/app/chat/chat-store.service';
import { Chat } from 'src/app/chat/chat';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { SelectItem } from 'src/app/shared/SelectItem';
import { ToastrService } from 'ngx-toastr';
import { SalesStoreService } from 'src/app/sales/sales-store.service';
import { Sale } from 'src/app/sales/sale';
import { LocalDateService } from 'src/app/core/local-date.service';
import { MenuItem, MessageService } from 'primeng/api';

@Component({
  selector: 'app-account-view',
  templateUrl: './account-view.component.html',
  styleUrls: ['./account-view.component.scss']
})
export class AccountViewComponent implements OnInit {
  account: Account;
  chats: Chat[];
  months: SelectItem[];
  years: SelectItem[];
  year: number;
  month: number;
  amount: number;
  new_password: string;
  new_password_confirm: string;
  blocked = false;
  sale: Sale;
  items: MenuItem[];
  constructor(
    public ngxSmartModalService: NgxSmartModalService,
    private accountStore: AccountStoreService,
    private route: ActivatedRoute,
    private interactions: ChatStoreService,
    private toastr: ToastrService,
    private saleStore: SalesStoreService,
    private router: Router,
    private localDates: LocalDateService, private messageService: MessageService
  ) {

    this.items = [
      {
        label: 'Settings',
        items: [
          { label: 'Edit', icon: 'pi pi-user-edit' },
          { label: 'Change Password', icon: 'pi pi-lock', command: () => this.openModal('passwordModal') },
          { label: 'Set Target', icon: 'mdi mdi-target', command: () => this.openModal('targetModal') },
          { label: 'Login', icon: 'pi pi-lock-open' },

        ]
      },
      {
        label: 'Delete Permanently',
        items: [
          { label: 'Delete', icon: 'pi pi-trash', command: () => this.openModal('deleteModal') },


        ]
      }
    ]

  }
  openModal = modal => {
    this.ngxSmartModalService.getModal(modal).open();
  }
  changePassword() {
    this.blocked = true;
    this.ngxSmartModalService.closeAll();
    if (this.account.Id) {
      this.accountStore.changePassword(this.account.Id, this.new_password, this.new_password_confirm).then((result) => {
        if (result) {
          this.messageService.add({ severity: 'success', summary: 'Saved', detail: 'Password changed successfully' });
        } else {
          this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Password could not be changed. Make sure your password matches confirm password and try again.' });
        }
        this.blocked = false;

      }).catch((error) => {
        this.blocked = false;
      })
    }
  }
  saveTarget() {
    this.blocked = true;
    if ((!this.month || this.month < 0)) {
      this.toastr.error('Please enter a valid month');
      return;
    }
    if ((!this.year || this.year < 0)) {
      this.toastr.error('Please enter a valid year');
      return;
    }
    if ((!this.amount || this.amount <= 0)) {
      this.toastr.error('Please enter a valid amount greater than 0');
      return;
    }
    const sale = new Sale();
    sale.TargetMonth = this.month;
    sale.TargetYear = this.year;
    sale.amount = this.amount;
    sale.account_id = this.account.Id;
    this.ngxSmartModalService.getModal('targetModal').close();
    this.saleStore.addSale(sale).subscribe(s => {
      this.toastr.success('Target saved succesfuly', 'Target Set');
      this.blocked = false;
    });

  }
  login = () => {
    this.blocked = true;
  }
  deleteAccount() {
    this.blocked = true;
    this.accountStore.deleteAccount(this.account.Id).then(r => {
      if (r) {
        this.ngxSmartModalService.closeAll();
        this.messageService.add({ severity: 'success', summary: 'Account deleted', detail: 'Account deleted successfully' });
        this.blocked = false;
        setTimeout(() => {
          this.router.navigateByUrl('/app/accounts');
        }, 300)
      } else {
        this.blocked = false;
      }
    }).catch((error) => {
      this.blocked = false;
    })
  }
  fetchSale() {
    if (this.account.role === 2) {
      this.saleStore.getSales().subscribe(sales => {

        // tslint:disable-next-line: variable-name
        const ___sale___ = sales.filter(sale => {
          return sale.account.Id === this.account.Id && sale.TargetMonth === new Date().getMonth() + 1;
        });

        if (___sale___ && ___sale___.length) {
          this.sale = ___sale___[0];
        }
      });
    }
  }
  ngOnInit() {

    this.months = this.localDates.getMonths();
    this.years = this.localDates.getYears();
    this.month = new Date().getMonth() + 1;
    this.year = new Date().getFullYear();
    this.route.paramMap.subscribe(p => {
      const id = parseFloat(p.get('id'));
      this.accountStore.getAccount(id).subscribe(account => {
        this.account = account;
        this.fetchSale();
      });
      this.interactions.getAccountMessages(id).subscribe(c => this.chats = c);
    });
  }

}
