import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { Account } from './account';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountStoreService {
  baseurl = environment.api;
  constructor(private http: HttpClient) { }
  getAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>(this.baseurl + '/api/accounts', { withCredentials: true });
  }
  changePassword(account_id: number, password: string, confirm_password: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      var data = {
        account_id: account_id,
        new_password: password,
        new_password_confirm: confirm_password
      }
      this.http.post(this.baseurl + '/api/changepassword', data, { withCredentials: true }).toPromise().then(res => {
        resolve(true)
      }).catch((error) => reject(error))
    })
  }
  deleteAccount(account_id): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.delete(this.baseurl + '/api/accounts/' + account_id, { withCredentials: true }).toPromise().then((response) => {
        resolve(true)
      }).catch((error) => reject(error))
    })

  }
  addAccount(account: Account, password: string): Observable<Account> {
    const data = {
      account,
      password
    };
    return this.http.post<Account>(this.baseurl + '/api/accounts', data, { withCredentials: true });
  }
  getAccount(id: number): Observable<Account> {
    return this.http.get<Account>(this.baseurl + '/api/accounts/' + id, { withCredentials: true });
  }
}
