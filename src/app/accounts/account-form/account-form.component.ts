import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'src/app/shared/SelectItem';
import { Account } from '../account';
import { AccountStoreService } from '../account-store.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.scss']
})
export class AccountFormComponent implements OnInit {
  accountTypes: SelectItem[];
  account: Account;
  password: string;
  passwordConfirm: string;
  constructor(private accountStore: AccountStoreService, private router: Router, private toastr: ToastrService) { }
  save() {
    this.accountStore.addAccount(this.account, this.password).subscribe(a => {
      this.account = this.account;
      this.toastr.success('Account created', 'Done');
      this.router.navigateByUrl('/app/accounts/view/' + a.Id);
    });
  }
  ngOnInit() {
    this.account = new Account();
    this.accountTypes = Array<SelectItem>();
    this.accountTypes.push({
      id: 1,
      name: 'Admin'
    });
    this.accountTypes.push({
      id: 2,
      name: 'Sales Affiliate'
    });
    this.accountTypes.push({
      id: 3,
      name: 'Team'
    });
  }

}
