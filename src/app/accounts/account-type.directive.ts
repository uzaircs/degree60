import { Directive, OnInit, OnChanges, SimpleChanges, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appAccountType]'
})
export class AccountTypeDirective implements OnInit, OnChanges {
  @Input() appAccountType: number;
  constructor(private el: ElementRef) { }
  ngOnChanges(changes: SimpleChanges): void {
    this.attachLabel();
  }
  parseRole(role: number): string {
    if (role === 1) {
      return 'Admin';
    }
    if (role === 2) {
      return 'Sales User';
    }
    if (role === 3) {
      return 'Team';
    }
  }
  createLabel(text: string): HTMLSpanElement {
    const statusLabel = document.createElement('span');
    const css = 'label-pending';
    statusLabel.classList.add('label');
    statusLabel.classList.add(css);
    statusLabel.innerText = text;
    return statusLabel;
  }
  attachLabel() {
    this.el.nativeElement.innerHTML = this.createLabel(this.parseRole(this.appAccountType)).outerHTML;
  }
  ngOnInit(): void {
    this.attachLabel();
  }
}
