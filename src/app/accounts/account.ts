
enum Permissions {
    SUPER = 1,
    SALES,
    TEAM,
    NONE
}
export class Account {
    Id: number;
    name: string;
    phone: string;
    email: string;
    role: Permissions.NONE;
    created_at: Date;
}
