import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { MomentModule } from 'ngx-moment';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { FormsModule } from '@angular/forms';
import { AccountsListComponent } from './accounts-list/accounts-list.component';
import { AccountFormComponent } from './account-form/account-form.component';
import { AccountTypeDirective } from './account-type.directive';
import { AccountViewComponent } from './account-view/account-view.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { BlockUIModule } from 'primeng/blockui';
import { MenuModule } from 'primeng/menu';
import { ButtonModule } from 'primeng/button';
@NgModule({
  declarations: [AccountsListComponent, AccountFormComponent, AccountTypeDirective, AccountViewComponent],
  imports: [
    CommonModule,
    CoreModule,
    MomentModule,
    NgxDropzoneModule,
    NgxDatatableModule,
    RouterModule,
    NgScrollbarModule,
    NgSelectModule,
    FormsModule,
    MenuModule,
    ToastModule,
    BlockUIModule,
    ButtonModule,

    NgxSmartModalModule.forRoot()
  ],
  providers: [MessageService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AccountsModule { }
