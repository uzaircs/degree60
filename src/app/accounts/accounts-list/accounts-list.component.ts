import { Component, OnInit } from '@angular/core';
import { AccountStoreService } from '../account-store.service';
import { Account } from '../account';


@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss']
})
export class AccountsListComponent implements OnInit {
  accounts: Account[];
  constructor(private accountService: AccountStoreService) { }

  ngOnInit() {
    this.accountService.getAccounts().subscribe(a => this.accounts = a);
  }

}
